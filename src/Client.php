<?php

namespace AliSaleem\UKVD;

use AliSaleem\Curl\Request;
use AliSaleem\UKVD\Exceptions\ResponseException;
use AliSaleem\UKVD\Models\DataItems;
use AliSaleem\UKVD\Models\VehicleData;

class Client
{
    const VEHICLE_DATA_URL = 'https://uk1.ukvehicledata.co.uk/api/datapackage/VehicleData';

    const VRN_REGEX = '/(^[A-Z]{2}[0-9]{2}\s?[A-Z]{3}$)|(^[A-Z][0-9]{1,3}\s?[A-Z]{3}$)|(^[A-Z]{3}\s?[0-9]{1,3}[A-Z]$)|(^[0-9]{1,4}\s?[A-Z]{1,2}$)|(^[0-9]{1,3}\s?[A-Z]{1,3}$)|(^[A-Z]{1,2}\s?[0-9]{1,4}$)|(^[A-Z]{1,3}\s?[0-9]{1,3}$)/';

    /** @var string */
    private $apiKey;

    /** @var integer */
    private $version;

    /** @var bool */
    private $nullItems;

    public function __construct(string $apiKey, int $version = 2, bool $nullItems = true)
    {
        $this->apiKey = $apiKey;
        $this->version = $version;
        $this->nullItems = intval($nullItems);
    }

    private function makeRequest(): Request
    {
        return (new Request(self::VEHICLE_DATA_URL))
            ->setHeaders([
                'Accept' => 'application/json',
            ])
            ->setParameters([
                'auth_apikey'   => $this->apiKey,
                'v'             => $this->version,
                'api_nullitems' => $this->nullItems,
            ]);
    }

    private function validateVrn(string $vrn): void
    {
        $vrn = \strtoupper(\preg_replace('/\s/', '', $vrn));
//        if (!\preg_match(self::VRN_REGEX, $vrn)) {
//            throw new \InvalidArgumentException('The VRN provided is invalid');
//        }
    }

    public function getVehicleData(string $vrn): DataItems
    {
        $this->validateVrn($vrn);
        $response = $this->makeRequest()
            ->addParameter('key_VRM', $vrn)
            ->get()
            ->getBodyAsObject();
        if (isset(
                $response->Response->StatusCode,
                $response->Response->DataItems
            )
            && \strtolower($response->Response->StatusCode ?? null) !== 'success'
        ) {
            throw new ResponseException($response->Response->StatusCode . ': ' . $response->Response->StatusMessage);
        }

        return new DataItems($response->Response->DataItems);
    }
}
