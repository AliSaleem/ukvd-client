<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property int    $V5CCertificateCount
 * @property int    $PlateChangeCount
 * @property object $V5CCertificateList
 * @property int    $KeeperChangesCount
 * @property int    $VicCount
 * @property int    $ColourChangeCount
 * @property object $ColourChangeList
 * @property object $KeeperChangesList
 * @property object $PlateChangeList
 * @property object $VicList
 */
class VehicleHistory extends AbstractModel
{
}