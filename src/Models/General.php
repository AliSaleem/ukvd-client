<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property Engine $Engine
 * @property String $PowerDelivery
 * @property String $TypeApprovalCategory
 * @property String $DrivingAxle
 * @property object $DataVersionNumber
 * @property String $EuroStatus
 */
class General extends AbstractModel
{
}