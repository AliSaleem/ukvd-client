<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property object $UnladenWeight
 * @property String $RigidArtic
 * @property String $BodyShape
 * @property object $PayloadVolume
 * @property object $PayloadWeight
 * @property int    $Height
 * @property int    $NumberOfSeats
 * @property int    $KerbWeight
 * @property object $GrossTrainWeight
 * @property object $LoadLength
 * @property object $DataVersionNumber
 * @property object $WheelBase
 * @property int    $CarLength
 * @property int    $Width
 * @property int    $NumberOfAxles
 * @property object $GrossVehicleWeight
 * @property object $GrossCombinedWeight
 */
class Dimensions extends AbstractModel
{
}