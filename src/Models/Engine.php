<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property double $Stroke
 * @property String $PrimaryFuelFlag
 * @property int    $ValvesPerCylinder
 * @property String $Aspiration
 * @property int    $NumberOfCylinders
 * @property String $CylinderArrangement
 * @property String $ValveGear
 * @property String $Location
 * @property String $Description
 * @property int    $Bore
 * @property String $Make
 */
class Engine extends AbstractModel
{
}