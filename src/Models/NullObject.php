<?php

namespace AliSaleem\UKVD\Models;

class NullObject implements \JsonSerializable, \ArrayAccess
{
    public function __get($name)
    {
        return $this;
    }

    public function __toString()
    {
        return '';
    }

    public function jsonSerialize(): null
    {
        return null;
    }

    public function offsetExists($offset): bool
    {
        return false;
    }

    public function offsetGet($offset): static
    {
        return $this;
    }

    public function offsetSet($offset, $value): void
    {
        return;
    }

    public function offsetUnset($offset): void
    {
        return;
    }
}
