<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property Torque       $Torque
 * @property object       $DataVersionNumber
 * @property Power        $Power
 * @property MaxSpeed     $MaxSpeed
 * @property int          $Co2
 * @property Acceleration $Acceleration
 */
class Performance extends AbstractModel
{
}