<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property String $Range
 * @property String $FuelType
 * @property String $EngineCapacity
 * @property String $MarketSectorCode
 * @property String $CountryOfOrigin
 * @property String $ModelCode
 * @property String $ModelVariant
 * @property object $DataVersionNumber
 * @property int    $NumberOfGears
 * @property int    $NominalEngineCapacity
 * @property String $MarqueCode
 * @property String $Transmission
 * @property String $BodyStyle
 * @property String $SysSetupDate
 * @property String $Marque
 * @property String $CabType
 * @property object $TerminateDate
 * @property String $Series
 * @property int    $NumberOfDoors
 * @property String $DriveType
 */
class SmmtDetails extends AbstractModel
{
}