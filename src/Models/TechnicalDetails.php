<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property Dimensions  $Dimensions
 * @property General     $General
 * @property Performance $Performance
 * @property Consumption $Consumption
 */
class TechnicalDetails extends AbstractModel
{
}