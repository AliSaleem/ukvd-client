<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property object $VedCo2Emissions
 * @property object $MotDue
 * @property object $VedBand
 * @property object $VedCo2Band
 * @property object $TaxDue
 * @property object $Message
 * @property object $VehicleStatus
 */
class MotVed extends AbstractModel
{
}