<?php

namespace AliSaleem\UKVD\Models;

use JsonSerializable;

abstract class AbstractModel implements JsonSerializable
{
    use InjectAttributesTrait;

    /** @var array */
    protected $attributes = [];

    /** @var array */
    protected $hidden = [];

    public function __get($name)
    {
        if (method_exists($this, 'get' . ucfirst($name))) {
            return $this->{'get' . ucfirst($name)}();
        } elseif (!\array_key_exists($name, $this->attributes)) {
            return new NullObject();
        }

        return $this->attributes[$name];
    }

    public function __set($name, $value): void
    {
        $this->attributes[$name] = $value;
    }

    public function __isset($name): bool
    {
        return \array_key_exists($name, $this->attributes);
    }

    public function jsonSerialize(): array
    {
        $publicAttributeKeys = \array_diff(\array_keys($this->attributes), $this->hidden);

        $publicAttributes = [];
        foreach ($publicAttributeKeys as $key) {
            $publicAttributes[$key] = $this->attributes[$key];
        }

        return $publicAttributes;
    }
}
