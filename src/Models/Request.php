<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property String   $RequestGuid
 * @property String   $PackageId
 * @property int      $PackageVersion
 * @property int      $ResponseVersion
 * @property DataKeys $DataKeys
 */
class Request extends AbstractModel
{
}