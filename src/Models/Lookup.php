<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property String   $StatusCode
 * @property String   $StatusMessage
 * @property object[] $AdviceTextList
 */
class Lookup extends AbstractModel
{
}