<?php

namespace AliSaleem\UKVD\Models;

use stdClass;

trait InjectAttributesTrait
{
    public function __construct(stdClass $object)
    {
        foreach ((array)$object as $k => $v) {
            if (!is_scalar($v) && empty($v)) {
                continue;
            }
            if (is_scalar($v)) {
                $this->$k = $v;
            } elseif (class_exists('\\AliSaleem\\UKVD\\Models\\' . $k)) {
                $className = '\\AliSaleem\\UKVD\\Models\\' . $k;
                $this->$k = new $className($v);
            } elseif (is_object($v) || (is_array($v) && $v)) {
                $this->$k = $v;
            }
        }
    }
}
