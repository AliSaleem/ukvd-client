<?php

namespace AliSaleem\UKVD\Models;

/**
 * @property TechnicalDetails      $TechnicalDetails
 * @property ClassificationDetails $ClassificationDetails
 * @property VehicleHistory        $VehicleHistory
 * @property VehicleStatus         $VehicleStatus
 * @property VehicleRegistration   $VehicleRegistration
 * @property SmmtDetails           $SmmtDetails
 */
class DataItems extends AbstractModel
{
}