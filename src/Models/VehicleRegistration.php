<?php

namespace AliSaleem\UKVD\Models;

use DateTime;

/**
 * @property DateTime $DateOfLastUpdate
 * @property String   $Colour
 * @property object   $AbiBrokerNetCode
 * @property String   $VehicleClass
 * @property String   $EngineNumber
 * @property String   $EngineCapacity
 * @property String   $TransmissionCode
 * @property String   $DtpMakeCode
 * @property boolean  $Exported
 * @property String   $YearOfManufacture
 * @property object   $WheelPlan
 * @property object   $DateExported
 * @property boolean  $Scrapped
 * @property String   $Transmission
 * @property DateTime $DateFirstRegisteredUk
 * @property String   $Model
 * @property int      $GearCount
 * @property boolean  $ImportNonEu
 * @property String   $DtpModelCode
 * @property object   $PreviousVrmGb
 * @property int      $GrossWeight
 * @property String   $DoorPlanLiteral
 * @property String   $MvrisModelCode
 * @property String   $Vin
 * @property String   $Vrm
 * @property DateTime $DateFirstRegistered
 * @property object   $DateScrapped
 * @property String   $DoorPlan
 * @property String   $VinLast5
 * @property boolean  $VehicleUsedBeforeFirstRegistration
 * @property int      $MaxPermissibleMass
 * @property String   $Make
 * @property String   $MakeModel
 * @property String   $TransmissionType
 * @property object   $SeatingCapacity
 * @property String   $FuelType
 * @property object   $Co2Emissions
 * @property boolean  $Imported
 * @property String   $MvrisMakeCode
 * @property object   $PreviousVrmNi
 * @property object   $VinConfirmationFlag
 */
class VehicleRegistration extends AbstractModel
{
}